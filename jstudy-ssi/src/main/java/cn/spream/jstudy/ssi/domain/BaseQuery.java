package cn.spream.jstudy.ssi.domain;

import cn.spream.jstudy.ssi.common.PaginatedList;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-30
 * Time: 下午1:25
 * To change this template use File | Settings | File Templates.
 */
public class BaseQuery {

    private int pageIndex;
    private int pageSize;
    private int startIndex;
    private int startRow;
    private int endRow;

    public BaseQuery() {
    }

    public BaseQuery(PaginatedList paginatedList) {
        this.pageIndex = paginatedList.getPageIndex();
        this.pageSize = paginatedList.getPageSize();
        this.startRow = paginatedList.getStartRow();
        this.endRow = paginatedList.getEndRow();
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }
}
