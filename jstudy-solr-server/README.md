solr环境搭建：
1.下载solr，地址：http://www.apache.org/dyn/closer.cgi/lucene/solr/4.6.0
2.将\solr-4.6.0\example\webapps\solr.war解压缩，把里面的内容拷贝到项目的webapp目录下
3.将\solr-4.6.0\example\multicore里面的内容拷贝到resources/solr目录里面，可根据自己情况配置
4.配置pom.xml中的zookeeper
5.配置tomcat启动参数：
    第一台机器：-DhostPort=8180 -Dbootstrap_conf=true -DnumShards=2 -Dsolr.data.dir=f:\solr\user\data1
    第二台机器：-DhostPort=8280 -Dsolr.data.dir=f:\solr\user\data2