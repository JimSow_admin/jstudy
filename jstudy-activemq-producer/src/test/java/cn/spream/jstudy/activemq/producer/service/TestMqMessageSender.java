package cn.spream.jstudy.activemq.producer.service;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-8-14
 * Time: 下午5:46
 * To change this template use File | Settings | File Templates.
 */
public class TestMqMessageSender {

    private MqMessageSender mqMessageSender;

    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-producer.xml");
        mqMessageSender = (MqMessageSender) context.getBean("mqMessageSender");
    }

    /**
     *
     */
    @Test
    public void testSend() {
        mqMessageSender.send("Hello World!");
    }

}
